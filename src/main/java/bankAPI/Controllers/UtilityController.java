package bankAPI.Controllers;

import bankAPI.Services.Constants;
import bankAPI.Services.Utility;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Daniel on 8/18/2017.
 */
@CrossOrigin(origins = "*")
@RestController
public class UtilityController {

    @Autowired
    private Utility utilities;
    private final static Logger logger = LoggerFactory.getLogger(UtilityController.class);

    @RequestMapping(value = "/summary")
    public  ResponseEntity<JSONObject> summary()
    {
        return new ResponseEntity<JSONObject>(utilities.getAggregate(), HttpStatus.OK);
    }

    @RequestMapping(value = "/transactions")
    public  ResponseEntity<List<JSONObject>> transactions()
    {
        return new ResponseEntity<List<JSONObject>>(utilities.getTransactions(), HttpStatus.OK);
    }

    @RequestMapping(value = "/getSettings")
    public  ResponseEntity<JSONObject> getSettings()
    {
        return new ResponseEntity<JSONObject>(Constants.getJSONSettings(),HttpStatus.OK);
    }

    @RequestMapping(value = "/setSettings",method = RequestMethod.POST)
    public  ResponseEntity setSettings(@RequestBody JSONObject data)
    {
        utilities.setLimits(data);
        logger.info("*BANKING LIMITS CHANGED*");
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
