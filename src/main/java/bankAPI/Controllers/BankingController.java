package bankAPI.Controllers;

import bankAPI.Services.BankingServices;
import bankAPI.Services.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
public class BankingController {

    @Autowired
    private BankingServices services;

    @RequestMapping("/balance")
    public ResponseEntity balance()
    {
        return  services.Balance();
    }

    @RequestMapping("/deposit")
    public ResponseEntity deposit(@RequestParam(value="credit", defaultValue="0") Double credit)
    {
        return  services.Deposit(credit);
    }

    @RequestMapping("/withdraw")
    public ResponseEntity withdraw(@RequestParam(value="debit", defaultValue="0") Double debit)
    {
        return  services.Withdraw(debit);
    }
}
