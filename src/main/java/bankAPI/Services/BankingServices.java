package bankAPI.Services;

/**
 * Created by Daniel on 8/14/2017.
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Repository
public class BankingServices  {
    private final static Logger logger = LoggerFactory.getLogger(BankingServices.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public ResponseEntity<String> Balance(){
        Date date = new Date();
        String response = String.format(Constants.SUCCESSFUL_BALANCE,date,getBalance());
        logger.info("*BALANCE* : " +response );
        return new ResponseEntity<String>(response, HttpStatus.OK);
    }

    public ResponseEntity<String> Deposit(Double credit){
        Date date = new Date();

        if(credit <= 0){
            String response = String.format(Constants.TRANSACTION_VALUE_INVALID, date, credit);
            logger.info("*DEPOSIT|FAIL* : " + response);
            return new ResponseEntity<String>(response, HttpStatus.EXPECTATION_FAILED);
        }
        else if(credit > Constants.MAX_TRANSACTION_CREDIT){
            String response = String.format(Constants.MAX_TRANSACTION_CREDIT_ERROR, date, credit);
            logger.info("*DEPOSIT|FAIL* : "+ response);
            return new ResponseEntity<String>(response, HttpStatus.EXPECTATION_FAILED);
        }
        else if(checkDailyDepositLimitCountReached()){
            String response = String.format(Constants.MAX_TRANSACTION_CREDIT_DAILY_COUNT_ERROR, date);
            logger.info("*DEPOSIT|FAIL* : "+ response);
            return new ResponseEntity<String>(response, HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS);
        }
        else if(checkDailyDepositLimitTotalReached()){
            String response = String.format(Constants.MAX_TRANSACTION_CREDIT_DAILY_TOTAL_ERROR, date);
            logger.info("*DEPOSIT|FAIL* : "+ response);
            return new ResponseEntity<String>(response, HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS);
        }
        else{
            Double balance = creditAccount(credit);
            String response = String.format(Constants.SUCCESSFUL_DEPOSIT, date, credit, balance);
            logger.info("*DEPOSIT|SUCCESS* : " + response);
            return new ResponseEntity<String>(response, HttpStatus.OK);
        }
    }

    public ResponseEntity<String> Withdraw(Double debit){
        Date date = new Date();

        if(debit < 0){
            String response = String.format(Constants.TRANSACTION_VALUE_INVALID, date, debit);
            logger.info("*WITHDRAW|FAIL* : "+ response);
            return new ResponseEntity<String>(response, HttpStatus.EXPECTATION_FAILED);
        }
        else if(debit > Constants.MAX_TRANSACTION_DEBIT){
            String response = String.format(Constants.MAX_TRANSACTION_DEBIT_ERROR, date, debit);
            logger.info("*WITHDRAW|FAIL* : "+ response);
            return new ResponseEntity<String>(response, HttpStatus.EXPECTATION_FAILED);
        }
        else if(checkDailyWithdrawLimitCountReached()){
            String response = String.format(Constants.MAX_TRANSACTION_DEBIT_DAILY_COUNT_ERROR, date);
            logger.info("*WITHDRAW|FAIL* : "+ response);
            return new ResponseEntity<String>(response, HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS);
        }
        else if(checkDailyWithdrawLimitTotalReached()){
            String response = String.format(Constants.MAX_TRANSACTION_DEBIT_DAILY_TOTAL_ERROR, date);
            logger.info("*WITHDRAW|FAIL* : "+ response);
            return new ResponseEntity<String>(response, HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS);
        }
        else{
            Double balance = debitAccount(debit);
            if(balance == null) {
                String response = String.format(Constants.INSUFFICIENT_FUNDS, date, debit);
                logger.info("*DEPOSIT|FAIL* : " + response);
                return new ResponseEntity<String>(response, HttpStatus.EXPECTATION_FAILED);
            }
            else {
                String response = String.format(Constants.SUCCESSFUL_WITHDRAWAL, date, debit, balance);
                logger.info("*DEPOSIT|SUCCESS* : " + response);
                return new ResponseEntity<String>(response, HttpStatus.OK);
            }
        }
    }

    private boolean checkDailyWithdrawLimitTotalReached(){
        Double sum = 0.0;
        for(Map map : getWithdrawalsToday()) {
            sum = sum + (Double) map.get("debit");
        }
        return  (sum > Constants.MAX_DAILY_TRANSACTION_DEBIT);
    }

    private boolean checkDailyDepositLimitTotalReached(){
        Double sum = 0.0;
        for(Map map : getDepositsToday()) {
            sum = sum + (Double) map.get("credit");
        }
        return  (sum > Constants.MAX_DAILY_TRANSACTION_CREDIT);
    }

    private boolean checkDailyDepositLimitCountReached(){
        return (getDepositsToday().size() >= Constants.MAX_DAILY_DEPOSIT_COUNT);
    }

    private boolean checkDailyWithdrawLimitCountReached(){
        return (getWithdrawalsToday().size() >= Constants.MAX_DAILY_WITHDRAW_COUNT);
    }

    @Transactional
    private Double creditAccount(Double credit) {
        String DEPOSIT_SQL = "INSERT INTO transactions (credit,balance) VALUES (?,?);";

        Double balance = getBalance()+ credit;
        jdbcTemplate.update(DEPOSIT_SQL, new Object[]{credit, balance});
        logger.info("*CREDIT* : +" + String.valueOf(credit));

        return balance;
    }

    @Transactional
    private Double debitAccount(Double debit) {
        String WITHDRAW_SQL = "INSERT INTO transactions (debit,balance) VALUES (?,?);";

        Double balance =  getBalance() - debit;
        if(balance >= 0) {
            jdbcTemplate.update(WITHDRAW_SQL, new Object[]{debit, balance});
            logger.info("*DEBIT* : +" + String.valueOf(debit));
            return balance;
        }
        else
            return null;
    }

    @Transactional
    private Double getBalance() {
        Date date = new Date();
        Double accountBalance = 0.0;
        String BALANCE_SQL = "SELECT balance FROM transactions ORDER BY id DESC LIMIT 1;";

        try {
            accountBalance = (Double) jdbcTemplate.queryForObject(BALANCE_SQL, Double.class);
        }catch (EmptyResultDataAccessException e){
            logger.info("*BALANCE* : Empty account detected ("+date+")");
        }
        finally {
            logger.info("*BALANCE* : " + accountBalance +" ("+date+")");
        }
        return accountBalance;

    }

    @Transactional
    public List<Map<String, Object>> getDepositsToday() {
        String TODAY_DEPOSIT_SQL = "SELECT * FROM transactions WHERE (credit IS NOT NULL) AND (DATE(timeStamp) = CURDATE());";

        List<Map<String, Object>> deposits = null;
        try {
            deposits = jdbcTemplate.queryForList(TODAY_DEPOSIT_SQL);
        }catch (Exception e){
            return null;
        }
        return deposits;

    }

    @Transactional
    public List<Map<String, Object>> getAllDeposits() {
        String ALL_DEPOSIT_SQL = "SELECT * FROM transactions WHERE (credit IS NOT NULL);";

        List<Map<String, Object>> deposits = null;
        try {
            deposits = jdbcTemplate.queryForList(ALL_DEPOSIT_SQL);
        }catch (Exception e){
            return null;
        }
        return deposits;

    }

    @Transactional
    public List<Map<String, Object>> getWithdrawalsToday() {
        String TODAY_DEPOSIT_SQL = "SELECT * FROM transactions WHERE (debit IS NOT NULL) AND (DATE(timeStamp) = CURDATE());";

        List<Map<String, Object>> withdrawals = null;
        try {
            withdrawals = jdbcTemplate.queryForList(TODAY_DEPOSIT_SQL);
        }catch (Exception e){
            return null;
        }
        return withdrawals;

    }

    @Transactional
    public List<Map<String, Object>> getAllWithdrawals() {
        String ALL_DEPOSIT_SQL = "SELECT * FROM transactions WHERE (debit IS NOT NULL);";

        List<Map<String, Object>> withdrawals = null;
        try {
            withdrawals = jdbcTemplate.queryForList(ALL_DEPOSIT_SQL);
        }catch (Exception e){
            return null;
        }
        return withdrawals;

    }

    @Transactional
    public List<Map<String, Object>> getAllTransactions() {
        String ALL_TRANSACTIONS_SQL = "SELECT * FROM transactions ORDER BY id;";

        List<Map<String, Object>> transactions = null;
        try {
            transactions = jdbcTemplate.queryForList(ALL_TRANSACTIONS_SQL);
        }catch (Exception e){
            return null;
        }
        return transactions;

    }



}
