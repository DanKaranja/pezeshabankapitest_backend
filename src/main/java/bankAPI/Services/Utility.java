package bankAPI.Services;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Daniel on 8/18/2017.
 */
@Repository
public class Utility {

    @Autowired
    private BankingServices services;

    public Utility() {
    }

    public List<JSONObject> getTransactions(){
        List<JSONObject> transactions = new ArrayList<>();
        for(Map map : services.getAllTransactions()) {
            JSONObject transaction = new JSONObject();
            //Timestamp timestamp = ((Timestamp) map.get("timeStamp"));
            transaction.put("timestamp",(new Date(((Timestamp) map.get("timeStamp")).getTime())).toString());
            transaction.put("credit",String.format("$%,.2f", (Double) map.get("credit")));
            transaction.put("debit",String.format("$%,.2f", (Double) map.get("debit")));
            transaction.put("balance",String.format("$%,.2f", (Double) map.get("balance")));
            transactions.add(transaction);
        }
        return transactions;
    }

    public JSONObject getAggregate(){
        JSONObject reply = new JSONObject();

        Double totalWithdrawals = 0.0;
        Double totalDeposits = 0.0;
        Double todayWithdrawals = 0.0;
        Double todayDeposits = 0.0;

        for(Map map : services.getAllWithdrawals()) {
            totalWithdrawals = totalWithdrawals + (Double) map.get("debit");
        }
        for(Map map : services.getAllDeposits()) {
            totalDeposits = totalDeposits + (Double) map.get("credit");
        }
        for(Map map : services.getWithdrawalsToday()) {
            todayWithdrawals = todayWithdrawals + (Double) map.get("debit");
        }
        for(Map map : services.getDepositsToday()) {
            todayDeposits = todayDeposits + (Double) map.get("credit");
        }

        reply.put("totalDeposits", totalDeposits);
        reply.put("totalWithdrawals", totalWithdrawals);
        reply.put("todayWithdrawals", todayWithdrawals);
        reply.put("todayDeposits", todayDeposits);

        return reply;
    }

    public void setLimits(JSONObject newValues){
        Constants.MAX_TRANSACTION_CREDIT = Double.valueOf((String) newValues.get("MAX_TRANSACTION_CREDIT"));
        Constants.MAX_DAILY_TRANSACTION_CREDIT = Double.valueOf((String) newValues.get("MAX_DAILY_TRANSACTION_CREDIT"));
        Constants.MAX_TRANSACTION_DEBIT = Double.valueOf((String) newValues.get("MAX_TRANSACTION_DEBIT"));
        Constants.MAX_DAILY_TRANSACTION_DEBIT = Double.valueOf((String) newValues.get("MAX_DAILY_TRANSACTION_DEBIT"));
        Constants.MAX_DAILY_DEPOSIT_COUNT = Double.valueOf((String) newValues.get("MAX_DAILY_DEPOSIT_COUNT"));
        Constants.MAX_DAILY_WITHDRAW_COUNT = Double.valueOf((String) newValues.get("MAX_DAILY_WITHDRAW_COUNT"));
    }

}
