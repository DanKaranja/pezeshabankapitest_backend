package bankAPI.Services;

import org.json.simple.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Daniel on 8/16/2017.
 */
public class Constants {

    public static final String SUCCESSFUL_DEPOSIT = "%s : You have successfully deposited $%,.2f into your account. Your new account balance is $%,.2f";
    public static final String SUCCESSFUL_WITHDRAWAL = "%s : You have successfully withdrawn $%,.2f from your account. Your new account balance is $%,.2f";
    public static final String SUCCESSFUL_BALANCE = "%s : Your current account balance is $%,.2f";

    public static final String TRANSACTION_VALUE_INVALID = "%s : Invalid Non-Positive Value (%,.2f)";
    public static final String INSUFFICIENT_FUNDS = "%s : You Have Insufficient Funds To Make This Transaction ($%,.2f)";

    public static final String MAX_TRANSACTION_CREDIT_DAILY_COUNT_ERROR = "%s : Exceeded Maximum Daily Number Of Deposit Transactions";
    public static final String MAX_TRANSACTION_DEBIT_DAILY_COUNT_ERROR = "%s : Exceeded Maximum Daily Number Of Withdrawals Transactions";

    public static final String MAX_TRANSACTION_CREDIT_DAILY_TOTAL_ERROR = "%s : Exceeded Maximum Daily Deposit Amount";
    public static final String MAX_TRANSACTION_DEBIT_DAILY_TOTAL_ERROR = "%s : Exceeded Maximum Daily Withdrawals Amount";

    public static final String MAX_TRANSACTION_CREDIT_ERROR = "%s : Exceeded Maximum Deposit Per Transaction ($%,.2f)";
    public static final String MAX_TRANSACTION_DEBIT_ERROR = "%s : Exceeded Maximum Withdrawal Per Transaction ($%,.2f)";

    public static Double MAX_TRANSACTION_CREDIT = 40000.00;
    public static Double MAX_DAILY_TRANSACTION_CREDIT = 150000.00;


    public static Double MAX_TRANSACTION_DEBIT = 20000.00;
    public static Double MAX_DAILY_TRANSACTION_DEBIT = 50000.00;



    public static Double MAX_DAILY_DEPOSIT_COUNT = 3.0;
    public static Double MAX_DAILY_WITHDRAW_COUNT = 3.0;


    public static JSONObject getJSONSettings(){
        JSONObject obj = new JSONObject();
        obj.put("MAX_TRANSACTION_CREDIT", MAX_TRANSACTION_CREDIT);
        obj.put("MAX_DAILY_TRANSACTION_CREDIT", MAX_DAILY_TRANSACTION_CREDIT);
        obj.put("MAX_TRANSACTION_DEBIT", MAX_TRANSACTION_DEBIT);
        obj.put("MAX_DAILY_TRANSACTION_DEBIT", MAX_DAILY_TRANSACTION_DEBIT);
        obj.put("MAX_DAILY_DEPOSIT_COUNT", MAX_DAILY_DEPOSIT_COUNT);
        obj.put("MAX_DAILY_WITHDRAW_COUNT", MAX_DAILY_WITHDRAW_COUNT);
        return obj;
    }
    public void saveValues(){
/*

        ClassLoader classLoader = getClass().getClassLoader();
        try {
                PrintWriter writer = new PrintWriter(new File(classLoader.getResource("settings/bankLimits.json").getPath()));
                writer.write(obj.toJSONString());
                writer.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }*/

    }

}
