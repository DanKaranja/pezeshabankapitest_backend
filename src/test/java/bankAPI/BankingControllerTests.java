/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bankAPI;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import bankAPI.Services.Constants;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class BankingControllerTests {

    @Autowired
    private MockMvc mockMvc;

    //Balance Tests
    @Test
    public void noParamBalanceShouldReturnExpectedBalance() throws Exception {

        this.mockMvc.perform(get("/balance")).andDo(print()).andExpect(status().isOk());
    }


    //Deposit Tests
    @Test
    public void paramInvalidDepositShouldReturnFailedExpectation() throws Exception {

        this.mockMvc.perform(get("/deposit").param("credit", "-1"))
                .andDo(print()).andExpect(status().isExpectationFailed());
    }

    @Test
    public void paramExcessDepositShouldReturnFailedExpectation() throws Exception {

        this.mockMvc.perform(get("/deposit").param("credit", String.valueOf(Constants.MAX_TRANSACTION_CREDIT + 1)))
                .andDo(print()).andExpect(status().isExpectationFailed());
    }

    //Withdrawal Tests
    @Test
    public void paramInvalidWithdrawalShouldReturnFailedExpectation() throws Exception {

        this.mockMvc.perform(get("/withdraw").param("debit", "-1"))
                .andDo(print()).andExpect(status().isExpectationFailed());
    }

    @Test
    public void paramExcessWithdrawalShouldReturnFailedExpectation() throws Exception {

        this.mockMvc.perform(get("/withdraw").param("debit", String.valueOf(Constants.MAX_TRANSACTION_DEBIT + 1)))
                .andDo(print()).andExpect(status().isExpectationFailed());
    }

    @Test
    public void paramInsufficientFundsShouldReturnFailedExpectation() throws Exception {

        this.mockMvc.perform(get("/withdraw").param("debit", String.valueOf(Double.MAX_VALUE)))
                .andDo(print()).andExpect(status().isExpectationFailed());

    }

    //Other

    @Test
    public void paramExcessTransactionsCountShouldReturnFailedExpectation() throws Exception {

        for(int i = 0; i < (Constants.MAX_DAILY_DEPOSIT_COUNT); i++){
            this.mockMvc.perform(get("/deposit").param("credit", "10"))
                    .andDo(print()).andExpect(status().isOk());
        }
        this.mockMvc.perform(get("/deposit").param("credit", "10"))
                .andDo(print()).andExpect(status().isUnavailableForLegalReasons());

        for(int i = 0; i < (Constants.MAX_DAILY_WITHDRAW_COUNT); i++){
            this.mockMvc.perform(get("/withdraw").param("debit", "10"))
                    .andDo(print()).andExpect(status().isOk());
        }
        this.mockMvc.perform(get("/withdraw").param("debit", "10"))
                .andDo(print()).andExpect(status().isUnavailableForLegalReasons());

    }

}
