# Pezesha Banking API Mini Porject (Spring Backend)


This is my submission for the java REST API backend. The application runs on the Spring framework. The API is exposed on port 8080. 

The application stores the transactions in a MySQL database hosted by freemysqlhosting.net. Below is the structure of the table

```sql
CREATE TABLE `transactions` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `timeStamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
 `debit` double DEFAULT NULL,
 `credit` double DEFAULT NULL,
 `balance` double DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=latin1
```

Simply clone the repo and run the application from your favorite  IDE (jdk : 1.8) or build an executable if you so wish to do so. Be sure to refresh gradle before you build and run.

The spring configuration file applicaiton.properties located in the resources folder contains the MySQL connection parameters. Change these fields if you wish to do so

```java
spring.datasource.url=[database url]
spring.datasource.username=[username]
spring.datasource.password=[password]
spring.datasource.driver-class-name=com.mysql.jdbc.Driver
```

## Banking Endpoints!


  - Balance - returns a response entity with the account's current balance
```
/balance
```
  - Deposit - Takes a double value and makes a deposit of that value
```
/deposit?credit=[Deposit Value]
```
  - Withdrawal - Takes a double value and makes a withdrawal of that value
```
/withdraw?debit=[withdrawal Value]
```
    
## Utility Endpoints!

  - Summary - Returns a JSON Object response entity with an aggregate summary of the account's transactions
```
/summary
```
  - Transactions - Returns a JSON Object response entity with an account statement
```
/transactions
```
  - Settings - Takes a double value and makes a withdrawal of that value
```
/getSettings
/setSettings
```



